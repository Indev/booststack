local booststack = booststack

local cv_slopeboostdecay = CV_RegisterVar({
    name = "_slopeboost_decay",
    defaultvalue = "0.004",
    possiblevalue = CV_Unsigned,
    flags = CV_NETVAR|CV_FLOAT,
})

local cv_slopeboostbrakemod = CV_RegisterVar({
    name = "_slopeboost_brakemod",
    defaultvalue = "0.01",
    possiblevalue = CV_Unsigned,
    flags = CV_NETVAR|CV_FLOAT,
})

local cv_speedboost = CV_RegisterVar({
    name = "_slopeboost_speedboost_max",
    defaultvalue = "1.0",
    possiblevalue = CV_Unsigned,
    flags = CV_NETVAR|CV_FLOAT,
})

local cv_speedboost_cap = CV_RegisterVar({
    name = "_slopeboost_speedboost_cap",
    defaultvalue = "1.0",
    possiblevalue = CV_Unsigned,
    flags = CV_NETVAR|CV_FLOAT,
})

local cv_accelboost = CV_RegisterVar({
    name = "_slopeboost_accelboost",
    defaultvalue = "1.0",
    possiblevalue = CV_Unsigned,
    flags = CV_NETVAR|CV_FLOAT,
})

booststack.registerBoostType("slopeboost", {
    stackable = true,
    stacktics = false,
    hidden = true,
    dynamic = true,

    getBoostPower = function(pmo)
        return pmo.slopeboost or 0, pmo.slopeaccel or 0
    end,
})

local function decayBoost(lastslopeboost, brake)
    return max(lastslopeboost - (cv_slopeboostdecay.value + brake + lastslopeboost/60), 0)
end

local function doBrake(pmo, lastslopeboost, brake)
    pmo.slopeboost = decayBoost(lastslopeboost, brake)
    pmo.slopeaccel = 0
    pmo.lastslopeboost = pmo.slopeboost
end

addHook("MobjThinker", function(pmo)
    if not booststack.running then return end

    local player = pmo.player
    local ks = player.kartstuff
    local slope = pmo.standingslope
    local lastslopeboost = pmo.lastslopeboost or 0
    local brake = 0

    if (player.cmd.buttons & BT_BRAKE) or ks[k_offroad] then
        brake = cv_slopeboostbrakemod.value
    end

    if not slope or (ks[k_offroad] and not (ks[k_sneakertimer] or ks[k_hyudorotimer] or ks[k_invincibilitytimer])) then
        doBrake(pmo, lastslopeboost, brake)
        return
    end

    local flip = pmo.eflags & MFE_VERTICALFLIP
    local momangle = R_PointToAngle2(0, 0, pmo.momx, pmo.momy)

    local hillangle = 0

    if ((slope.zangle > 0) and flip) or ((slope.zangle < 0) and (not flip)) then
        hillangle = momangle - slope.xydirection
    else
        hillangle = momangle - (slope.xydirection + ANGLE_180)
    end

    hillangle = max(abs(hillangle) - ANG1*3, 0) -- ANG1*3 somehow fixes some slopes???

    if hillangle >= ANGLE_90 then
        doBrake(pmo, lastslopeboost, brake)
        return
    end

    local anglemult = FixedDiv(AngleFixed(ANGLE_90-hillangle), 90*FRACUNIT)
    local slopemult = FixedDiv(AngleFixed(min(abs(slope.zangle)+ANG1*3, ANGLE_90)), 90*FRACUNIT)

    local mult = FixedMul(anglemult, slopemult)

    local speedboost = min(FixedMul(mult, cv_speedboost.value), cv_speedboost_cap.value)
    local accelboost = FixedMul(mult, cv_accelboost.value)

    if player.cmd.buttons & BT_BRAKE then
        speedboost = decayBoost(lastslopeboost, brake)
    elseif speedboost >= lastslopeboost then
        speedboost = min(speedboost, lastslopeboost+mult*5/100)
    else
        speedboost = max(decayBoost(lastslopeboost, brake), speedboost)
    end

    pmo.slopeboost = speedboost
    pmo.slopeaccel = accelboost

    pmo.lastslopeboost = speedboost
end, MT_PLAYER)

addHook("MobjThinker", function(pmo)
    if booststack.running and (pmo.slopeboost or pmo.slopeaccel) then booststack.doBoost(pmo, "slopeboost", 1) end
end, MT_PLAYER)
