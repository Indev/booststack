local booststack = booststack

-- Vanilla boost types

-- Sneaker boost
booststack.registerBoostType("sneakerboost", {
    maxgrade = 3,
    speedboost = "0.2756", -- Hard speed sneaker power, (17294+768)/FRACUNIT (see k_kart.c)
    accelboost = "8",
})

-- Invincibility boost
booststack.registerBoostType("invincibilityboost", {
    speedboost = "0.375",
    accelboost = "3",
    stackable = false,
})

-- Grow boost
booststack.registerBoostType("growboost", {
    speedboost = "0.2",
    accelboost = "0",
    stackable = false,
})

-- Drift boost
booststack.registerBoostType("driftboost", {
    speedboost = "0.25",
    accelboost = "4",
})

-- Start boost
booststack.registerBoostType("startboost", {
    speedboost = "0.25",
    accelboost = "6.0",
})

addHook("MobjThinker", function(pmo)
    if not booststack.running then return end

    local ks = pmo.player.kartstuff

    if ks[k_driftboost] and (pmo.driftboost == nil or pmo.driftboost < ks[k_driftboost]) then
        booststack.doBoost(pmo, "driftboost", ks[k_driftboost])
    end

    if ks[k_sneakertimer] and (pmo.sneakertimer == nil or pmo.sneakertimer < ks[k_sneakertimer]) then
        booststack.doBoost(pmo, "sneakerboost", ks[k_sneakertimer])
    elseif pmo.player.kartstuff[k_sneakertimer] and pmo.sneakertimer == ks[k_sneakertimer] then
        booststack.doBoost(pmo, "sneakerboost", ks[k_sneakertimer], true)
    end

    if ks[k_invincibilitytimer] and (pmo.invincibilitytimer == nil or pmo.invincibilitytimer < ks[k_invincibilitytimer]) then
        booststack.doBoost(pmo, "invincibilityboost", pmo.player.kartstuff[k_invincibilitytimer])
    end

    if ks[k_growshrinktimer] > 0 and (pmo.growtimer == nil or pmo.growtimer < ks[k_growshrinktimer]) then
        booststack.doBoost(pmo, "growboost", ks[k_growshrinktimer])
    elseif ks[k_growshrinktimer] <= 0 and booststack.haveBoost(pmo, "growboost") then
        booststack.cancelBoost(pmo, "growboost")
    end

    if ks[k_startboost] and (pmo.startboost == nil or pmo.startboost < ks[k_startboost]) then
        booststack.doBoost(pmo, "startboost", ks[k_startboost])
    end

    if booststack.haveBoost(pmo, "sneakerboost") then
        ks[k_sneakertimer] = max(ks[k_sneakertimer], 1)
    end

    pmo.driftboost = pmo.player.kartstuff[k_driftboost]
    pmo.sneakertimer = pmo.player.kartstuff[k_sneakertimer]
    pmo.invincibilitytimer = pmo.player.kartstuff[k_invincibilitytimer]
    pmo.growtimer = pmo.player.kartstuff[k_growshrinktimer]
    pmo.startboost = pmo.player.kartstuff[k_startboost]
end, MT_PLAYER)

local function cancelBoost(player)
    if not (player.mo and player.mo.valid) then return end

    booststack.cancelBoost(player.mo, "driftboost")
    booststack.cancelBoost(player.mo, "sneakerboost")
    booststack.cancelBoost(player.mo, "startboost")
end

addHook("PlayerSpin", cancelBoost)
addHook("PlayerExplode", cancelBoost)
addHook("PlayerSquish", cancelBoost)
