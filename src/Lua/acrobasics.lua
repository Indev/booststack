local booststack = booststack

local cv_acrostack = CV_RegisterVar({
    name = "booststack_acro_support",
    defaultvalue = "On",
    possiblevalue = CV_OnOff,
    flags = CV_NETVAR,
})

booststack.registerBoostType("acrobasicsboost", {
    stackable = false,
    hidden = true,
    dynamic = true,

    getBoostPower = function(pmo)
        local player = pmo.player

        local speedboost = player.trickbooststr
        local accelboost = player.finaltrickboost and 4*FRACUNIT or player.trickboost and 2*FRACUNIT or 0

        return speedboost, accelboost
    end,
})

addHook("MobjThinker", function(pmo)
    if not (booststack.running and cv_acrostack.value) then return end

    local p = pmo.player

    if p.trickboost and (pmo.as_trickboost == nil or pmo.as_trickboost < p.trickboost) then
        booststack.doBoost(pmo, "acrobasicsboost", p.trickboost)
    end

    pmo.as_trickboost = p.trickboost
end, MT_PLAYER)
