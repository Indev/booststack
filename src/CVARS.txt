Cvars added by booststack:

Netvars:
bool booststack - toggles booststack. Note that it will have effect only next race. Off by default.
float booststack_speedboostdropoff - Controls how much speedboost decreases over time if no boosts are active. 0.01 by default.
bool booststack_acro_support - Enables acrobasics boost, which kinda cancels slowdown from sneakers. On by default.

Local cvars:
bool booststack_debug_log - toggles debug messages in console

There are also quite a lot of "internal" netvars related to boosts. Check source code for them.