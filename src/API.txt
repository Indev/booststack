Lua API documentation for booststack

Tables:

BoostDef table:
{
    maxgrade = integer, -- Maximum grade for boost. See booststac.doBoost for more information
    stackable = boolean, -- Stackable boost combine speedboost, non-stackable pick max speedboost
    stacktics = boolean, -- Decides would time of this boost be stacked with other stackable boosts. Has no effect if boost is not stackable
    hidden = boolean, -- Would this boost be taken into account by booststack.stackedGradeSum (affects only visuals)
    dynamic = boolean, -- If boost is dynamic, booststack would use getBoostPower function to decide speedboost and accelboost
    getBoostPower = function(mobj_t pmo), -- Called every frame for dynamic boosts. Should return 2 values - speedboost and accelboost
    
    -- Used if boost is not dynamic
    speedboost = float (in form of string, ex "0.5"),
    accelboost = float (in form of string),
    
    think = function(mobj_t pmo, BoostData boost), -- Called every frame when boost is active
}

All values except functions are converted to cvars (mostly for debug)

BoostData table:
{
    grade = integer, -- Current boost grade
    info = NetsafeBoostDef, -- Basically, BoostDef table but with all cvars replaced with their values
    tics = tic_t, -- Timer for boost. Will not decrease if pmo.stacktics have greatter value
}

Functions:

-- Register new boost type
void booststack.registerBoostType(string name, BoostDef def)

-- Returns BoostDef converted to netsafe version which can be synched between clients.
-- If noerror is true, will return nil instead of throwing error if boost is not found
NetsafeBoostDef booststack.getBoostInfo(string name, bool noerror)

-- Returns iterator over player boosts. Use it instead of pairs for net safety
Iterator booststack.iterboosts(pmo)

-- Returns speedboost and accelboost for given player's boost. Mostly used internally
fixed_t, fixed_t booststack.getBoostPower(mobj_t pmo, BoostData boost)

-- Applies boost to player. If this boost is active, maximum of current boost tics and given tics is picked.
-- If noupgrade is true, boost grade is not increased.
void booststack.doBoost(mobj_t pmo, string name, tic_t tics, bool noupgrade)

-- Cancel boost if it is active
void booststack.cancelBoost(mobj_t pmo, string name)

-- Returns if player has at least 2 stackable boosts. Was used in early versions,
-- Will probably be removed
bool booststack.haveStackedBoost(mobj_t pmo)

-- Returns required boost data for player if the boost is active
BoostData booststack.getBoostData(mobj_t pmo, string name)

-- Returns true if player has the boost. Shortcut for booststack.getBoostData(pmo, name) ~= nil
bool booststack.haveBoost(mobj_t pmo, string name)

-- Returns total stack value for all stackable boosts player have. Mostly used for visuals
integer booststack.stackedGradeSum(mobj_t pmo)

Other:

-- True if booststack is active this race. Updated on mapload's
booststack.running = boolean