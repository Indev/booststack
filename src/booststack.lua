-- "It's time to reeeeeinvent the wheel!" (c)
-- ...Because "the wheel" is not reusable somehow.

local booststack = {
    boostinfo = {},
    boostnames = {}, -- To be network-safe when iterating over boosts (no pairs)
}

local function leveltimeAnimate(actor, frames, time_per_frame)
    actor.frame = states[actor.state].frame + (leveltime / time_per_frame) % frames
end

freeslot("S_BOOSTSTACKVFX", "SPR_BSSS")

local S_BOOSTSTACKVFX = S_BOOSTSTACKVFX
local vfxdist = 30*FRACUNIT
local vfxminscale = FRACUNIT
local vfxmaxscale = 2*FRACUNIT
local vfxscalediff = vfxmaxscale - vfxminscale

local function getVfxScale(boostcount)
    boostcount = min(boostcount, 4)

    return FixedMul(vfxminscale + FixedMul(vfxscalediff, FixedDiv(boostcount*FRACUNIT, 4*FRACUNIT)), mapobjectscale)
end

states[S_BOOSTSTACKVFX] = {
    sprite = SPR_BSSS,
    frame = A|FF_PAPERSPRITE|FF_TRANS50,
    tics = -1,
}

local cv_enabled = CV_RegisterVar({
    name = "booststack",
    defaultvalue = "Off",
    possiblevalue = CV_OnOff,
    flags = CV_NETVAR,
})

local cv_speedboostdropoff = CV_RegisterVar({
    name = "booststack_speedboostdropoff",
    defaultvalue = "0.01",
    possiblevalue = CV_Unsigned,
    flags = CV_NETVAR|CV_FLOAT,
})

booststack.cv_enabled = cv_enabled
booststack.running = cv_enabled.value

local cv_debug_log = CV_RegisterVar({
    name = "booststack_debug_log",
    defaultvalue = "Off",
    possiblevalue = CV_OnOff,
})

local function log(fmt, ...)
    print("[booststack] "..string.format(fmt, ...))
end

local function dprint(fmt, ...)
    if cv_debug_log.value then log(fmt, ...) end
end

local function getDefault(value, default)
    if value == nil then return default end

    return value
end

function booststack.registerBoostType(name, def)
    local boostinfo = {}
    
    -- God damn you "netvars have same netid" error
    local prefix = "_"..name

    boostinfo.maxgrade = CV_RegisterVar({
        name = prefix.."_maxgrade",
        defaultvalue = getDefault(def.maxgrade, 1),
        possiblevalue = CV_Unsigned,
        flags = CV_NETVAR,
    })

    boostinfo.stackable = CV_RegisterVar({
        name = prefix.."_stackable",
        defaultvalue = getDefault(def.stackable, true) and "Yes" or "No",
        PossibleValue = CV_YesNo,
        flags = CV_NETVAR,
    })
    
    boostinfo.stacktics = CV_RegisterVar({
        name = prefix.."_stacktics",
        defaultvalue = getDefault(def.stacktics, true) and "Yes" or "No",
        possiblevalue = CV_YesNo,
        flags = CV_NETVAR,
    })
    
    boostinfo.hidden = getDefault(def.hidden, false)

    boostinfo.dynamic = def.dynamic

    if not def.dynamic then
        boostinfo.speedboost = CV_RegisterVar({
            name = prefix.."_speedboost",
            defaultvalue = getDefault(def.speedboost, "0.5"),
            PossibleValue = CV_Unsigned,
            flags = CV_NETVAR|CV_FLOAT,
        })

        boostinfo.accelboost = CV_RegisterVar({
            name = prefix.."_accelboost",
            defaultvalue = getDefault(def.accelboost, "0.5"),
            possiblevalue = CV_Unsigned,
            flags = CV_NETVAR|CV_FLOAT,
        })
    else
        boostinfo.getBoostPower = def.getBoostPower or function(pmo) return FRACUNIT/2, FRACUNIT/2 end
    end
    
    boostinfo.think = def.think

    booststack.boostinfo[name] = boostinfo
    table.insert(booststack.boostnames, name)
    
    log("Add boost type %q", name)
end

function booststack.getBoostInfo(name, noerror)
    local boostinfo = booststack.boostinfo[name]

    if not (boostinfo or noerror) then error("Boost type "..name.." doesn't exist") end

    -- ffs turns out game can't send cvars userdata via net
    local netsafe = { name = name }
    
    netsafe.maxgrade = boostinfo.maxgrade.value
    
    netsafe.stackable = boostinfo.stackable.value
    
    netsafe.stacktics = boostinfo.stacktics.value
    
    netsafe.hidden = boostinfo.hidden
    
    netsafe.dynamic = boostinfo.dynamic
    
    if not boostinfo.dynamic then
        netsafe.speedboost = boostinfo.speedboost.value
        netsafe.accelboost = boostinfo.accelboost.value
    end
    
    return netsafe
end

function booststack.iterboosts(pmo)
    local i = 1

    local function nextboost()
        if not pmo.booststack then return end

        while i <= #booststack.boostnames do
            local name = booststack.boostnames[i]

            i = i + 1

            if pmo.booststack[name] ~= nil then return name, pmo.booststack[name] end
        end
    end

    return nextboost
end

function booststack.getBoostPower(pmo, boost)
    local speedboost, accelboost

    if boost.info.dynamic then
        local boostinfo = booststack.boostinfo[boost.info.name]
        speedboost, accelboost = boostinfo.getBoostPower(pmo, boost)
    else
        speedboost = boost.info.speedboost
        accelboost = boost.info.accelboost
    end

    return speedboost, accelboost
end

function booststack.doBoost(pmo, name, tics, noupgrade)
    if not name then error("Missing boost type name as argument #2 for booststack.doBoost") end
    if tics == nil then error("Missing boost tics as argument #3 for booststack.doBoost") end

    local boostinfo = booststack.getBoostInfo(name)

    pmo.booststack = pmo.booststack or {}

    if pmo.booststack[name] ~= nil then
        if not noupgrade then
            pmo.booststack[name].grade = min(pmo.booststack[name].grade + 1, pmo.booststack[name].info.maxgrade)
        end

        pmo.booststack[name].tics = max(pmo.booststack[name].tics, tics)

        dprint("Continue boost %q, grade=%d, tics=%d", name, pmo.booststack[name].grade, pmo.booststack[name].tics)
    else
        pmo.booststack[name] = {
            grade = 1,
            info = boostinfo,
            tics = tics,
        }

        dprint("Start boost %q, tics=%d", name, pmo.booststack[name].tics)
    end

    if boostinfo.stackable and boostinfo.stacktics then
        pmo.stacktics = max(pmo.stacktics or 0, tics)
    end
end

function booststack.cancelBoost(pmo, name)
    if not pmo.booststack then return end

    dprint("Cancel boost %q", name)

    pmo.booststack[name] = nil

    -- Reset stacktics to maximum tics value of all boosts
    if pmo.stacktics then
        local maxtics = 0

        for _, boost in booststack.iterboosts(pmo) do
            if boost.stackable and boost.stacktics then
                maxtics = max(maxtics, boost.tics)
            end
        end

        pmo.stacktics = maxtics
    end
end

function booststack.haveStackedBoost(pmo)
    if not pmo.booststack then return false end

    -- Kinda weird way to check but here how it works: on first iteration,
    -- have_boost would be false, so it would just be set to true. So, if we
    -- have more than one boost stacked, we go to next iteration => have_boost
    -- is true => function returns true
    local have_boost = false
    for _, boost in pairs(pmo.booststack) do
        if boost.info.stackable then
            if have_boost then return true end

            have_boost = true
        end
    end

    return false
end

function booststack.getBoostData(pmo, name)
    if not pmo.booststack then return end

    return pmo.booststack[name]
end

function booststack.haveBoost(pmo, name)
    return booststack.getBoostData(pmo, name) ~= nil
end

function booststack.stackedGradeSum(pmo)
    if not pmo.booststack then return 0 end

    local sum = 0

    for _, boost in booststack.iterboosts(pmo) do
        if boost.info.stackable and not boost.info.hidden then
            sum = sum + boost.grade
        end
    end

    return sum
end

local function GetKartBoostPower(pmo)
    if not booststack.running then return end

    local player = pmo.player
    
    local ks = player.kartstuff

    if not pmo.booststack then pmo.booststack = {} end
    if pmo.bs_lastframeboost == nil then pmo.bs_lastframeboost = 0 end

    local speedboost = 0
    local accelboost = 0

    local toremove = {}

    if pmo.stacktics then
        pmo.stacktics = pmo.stacktics - 1
    end

    local stacked = booststack.haveStackedBoost(pmo)

    local nostack_speedboost
    
    local stacked_speedboosts = {}

    for name, boost in booststack.iterboosts(pmo) do
        local boostinfo = booststack.boostinfo[name]
    
        if boost.tics then
            boost.tics = boost.tics - 1

            if boost.info.stackable and boost.info.stacktics then
                boost.tics = max(boost.tics, pmo.stacktics)
            end

            local cur_speedboost, cur_accelboost = booststack.getBoostPower(pmo, boost)

            if boost.info.stackable then
                for i = 1, boost.grade do
                    table.insert(stacked_speedboosts, cur_speedboost)
                end
            else
                nostack_speedboost = max(nostack_speedboost or 0, cur_speedboost)
            end

            accelboost = max(accelboost, cur_accelboost)
            
            if boostinfo.think then boostinfo.think(pmo, boost) end
        else
            pmo.booststack[name] = nil
        end
    end

    if ks[k_spinouttimer] and ks[k_wipeoutslow] == 1 then -- Slow down after you've been bumped
        ks[k_speedboost] = 0
        ks[k_accelboost] = 0
        return
    end
    
    -- table.sort sorts in reverse order (from what we need)
    table.sort(stacked_speedboosts)
    
    for i = 1, #stacked_speedboosts do
        -- We want the strongest boost to be least reduced. It would be at the end of list,
        -- so len - i (which equals len) + 1 would be 1, so it is not reduced at all.
        speedboost = speedboost + stacked_speedboosts[i]/min(#stacked_speedboosts - i + 1, 5)
    end

    speedboost = max(nostack_speedboost or 0, speedboost)

    -- value smoothing
    if ks[k_offroad] and not (ks[k_invincibilitytimer] or ks[k_hyudorotimer] or ks[k_sneakertimer]) then
        ks[k_speedboost] = max(speedboost, ks[k_speedboost])/2
        ks[k_accelboost] = max(accelboost, ks[k_accelboost])/2
    elseif speedboost >= pmo.bs_lastframeboost then
        ks[k_speedboost] = max(ks[k_speedboost], speedboost)
        ks[k_accelboost] = max(accelboost, ks[k_accelboost])
    else
        ks[k_speedboost] = max(pmo.bs_lastframeboost - cv_speedboostdropoff.value, player.kartstuff[k_speedboost])
        ks[k_accelboost] = max(accelboost, ks[k_accelboost])
    end
    
    pmo.bs_lastframeboost = ks[k_speedboost]
end

addHook("ThinkFrame", function()
    if leveltime == 2 then booststack.running = booststack.cv_enabled.value end
    if not booststack.running then return end
    
    for player in players.iterate do
        if player.mo and player.mo.valid then
            local pmo = player.mo
            
            GetKartBoostPower(pmo)
            
            local stacked = booststack.stackedGradeSum(pmo)
            
            -- Stacked effect
            if stacked > 1 then
                if not (player.boostvfx and player.boostvfx.valid) then
                    player.boostvfx = P_SpawnMobj(pmo.x, pmo.y, pmo.z, MT_THOK)
                end
                
                player.boostvfx.state = S_BOOSTSTACKVFX
                player.boostvfx.color = pmo.color
                player.boostvfx.destscale = getVfxScale(stacked)
                player.boostvfx.angle = pmo.angle + ANGLE_90
                leveltimeAnimate(player.boostvfx, 5, 3)
                
                local x = pmo.x + FixedMul(cos(pmo.angle), FixedMul(vfxdist, mapobjectscale))
                local y = pmo.y + FixedMul(sin(pmo.angle), FixedMul(vfxdist, mapobjectscale))
                local z = pmo.z
                
                P_MoveOrigin(player.boostvfx, x, y, z)
            elseif player.boostvfx and player.boostvfx.valid then
                player.boostvfx.state = S_INVISIBLE
                player.boostvfx.scale = getVfxScale(max(stacked, 2))
                P_SetOrigin(player.boostvfx, pmo.x, pmo.y, pmo.z)
            end
        end
    end
end)

addHook("NetVars", function(net)
    booststack.running = net(booststack.running)
end)

rawset(_G, "booststack", booststack)
